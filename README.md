# timee
A resuming timer for stand-up and jour-fixe meetings.

## Installation
This program is designed for GNU/Linux. You may be able to run it on other OSes though...

### Dependencies
If you want to use `timee` with sounds between slots, you need `ffmpeg` as `timee` will want to call `ffplay` or `avplay`.

For Ubuntu/Debian, you can install `ffmpeg` by issueing:
```
sudo apt install ffmpeg
```

### Quick Install
In order to just try out `timee`, you need [Pipenv](https://pipenv.readthedocs.io/en/latest/install/#pragmatic-installation-of-pipenv) to install the
virtual environment and all dependencies for you.

`timee` is written in Python 3.7, if you don't have it installed on your system, you can use
[pyenv](https://github.com/pyenv/pyenv-installer) (Pipenv supports automatic Python installs with pyenv) to install it.

Once you have Pipenv (and pyenv), you can install timee:

```
# Clone the timee repository from GitLab
git clone git@gitlab.com:paul.kernstock/timee.git

# Create virtual environment and install dependencies
cd timee
pipenv install

# Activate the virtual environment and use timee directly
pipenv shell 
./timee.py --help
```

### Full Install
For a full install where you can use `timee` as seamless as any other CLI in your terminal you can use [PyInstaller](https://www.pyinstaller.org/).
Currently, if you don't have Python 3.7 installed natively on your system (not as a pyenv install), you will have to
catch up on this now. On Ubuntu/Debian, this is done via `apt install python3.7-dev`.

Perform the installation with Pipenv (and pyenv) as described above.

```
# Activate the virtual environment
pipenv shell

# Install PyInstaller in the virtual environment
pip install pyinstaller

# Use PyInstaller to create a binary including the soundfiles
pyinstaller timee.py --clean --onefile --add-data sounds:sounds

# Link the created binary to the local binaries directory
ln -s /path/to/timee/dist/timee ${HOME}/.local/bin/

# and try it out!
timee -i 2
```

## Usage

```
usage: timee [-h] [-i INTERVAL] [-c M [M ...]]

A resuming timer for stand-up and jour-fixe meetings.

optional arguments:
  -h, --help            show this help message and exit
  -i INTERVAL, --interval INTERVAL
                        Duration of each timer interval in seconds. (default:
                        180)
  -c M [M ...], --crew M [M ...]
                        Members of the present crew. If not given, timee will
                        run indefinitely. (default: None)
```

## Copyright Notice
WAVE-files in the `sounds` directory are licensed under Creative Commons licenses as noted in the following:

- `toaster_oven_ding.wav` by sethlind on [freesound.org](https://freesound.org/people/sethlind/sounds/265012/), licensed under
[CC 0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)
- `fanfare.wav` by SpiceProgram on [freesound.org](https://freesound.org/people/SpiceProgram/sounds/365034/), licensed under
[CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)