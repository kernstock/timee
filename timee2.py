#!/usr/bin/env python

from datetime import timedelta
import time
from typing import List
import sys

import click
from pynput import keyboard


@click.command()
@click.option('-i', '--interval', help='Duration of each timer interval in seconds.', default=180)
def timee(interval):
    """
    A resuming timer for stand-up and jour-fixe meetings.
    """

    while True:
        continue_ = run_slot(interval=interval)
        if not continue_:
            click.echo()
            click.echo('Aborted!')
            break
        clear_output_line()


def run_slot(interval, current=None, next_=None):
    """
    Count down the time of a slot and handle commands
    :param interval: Interval length
    :param current: Speaker in this slot
    :param next_: Speaker in the next slot
    :return: True, if this session should be continued, else False
    """
    slot_time = timedelta(seconds=interval + 1)
    paused = False
    while slot_time:
        if not paused:
            slot_time = slot_time - timedelta(seconds=1)
        # Echo the slot message
        clear_output_line()
        if slot_time > timedelta(seconds=interval) * .1:
            background = 'green'
        elif not slot_time:
            background = 'red'
        else:
            background = 'yellow'
        slot_message = compile_slot_message(
            slot_time=slot_time, background_color=background, current=current, next_=next_, paused=paused)
        click.echo(f"{slot_message}", nl=False)
        # Handle key commands every second
        pressed_keys = check_control_input()
        if pressed_keys:
            if paused:
                if keyboard.Key.space in pressed_keys:
                    paused = False
            else:
                for key in pressed_keys:
                    # Alphanumeric keys
                    if type(key) == keyboard.KeyCode:
                        if key.char == 'q':
                            return False
                    # Special keys
                    elif type(key) == keyboard.Key:
                        if key == keyboard.Key.right:
                            return True
                        if key == keyboard.Key.space:
                            paused = True
    return True


def compile_slot_message(slot_time, background_color, current=None, next_=None, paused=False):
    """
    Compile and style the message to be printed during the slot
    :return Styled message
    """
    if current is None:
        slot_message = ''
        next_message = ''
    else:
        possessive = current + "'s" if current[-1] != 's' else current + "'"
        slot_message = f'{click.style(possessive, fg="green")} slot, '
        if next_ is None:
            next_message = " You'll be free."
        else:
            next_message = f' Next: {click.style(next_, fg="green")}.'
    time_slot_message = f'{click.style(" " + str(slot_time) + " ", fg="black", bg=background_color)}'
    if paused:
        paused_symbol = '|| '
    else:
        paused_symbol = ''
    return f"\r{paused_symbol}{slot_message}{time_slot_message} remaining.{next_message}"


def check_control_input() -> List[keyboard.Key]:
    """
    Sleep a second while listening for keyboard input.
    :return: Released keys
    """
    with keyboard.Events() as events:
        time.sleep(1)
        keys = []
        while True:
            # noinspection PyTypeChecker
            event = events.get(.001)
            if event is None:
                break
            # Only keep released keys
            if type(event) == keyboard.Events.Release:
                keys.append(event.key)
    return keys


def clear_output_line():
    sys.stdout.write('\033[2K\033[1G')


timee()
